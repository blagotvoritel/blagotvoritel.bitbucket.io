'use strict';

function isTouch(){
    return ('ontouchstart' in document.documentElement) ? true : false;
}

function initMobMenu(){
      var buttons = document.querySelectorAll("[data-toggle-menu]");
      var mobMenu = document.querySelector(".mobile-menu"); 
      var body = document.getElementsByTagName("body"); 
      var typeEvent = isTouch() ? "touchstart" : "click";
      var timeTransition = 150;
      if(!mobMenu) {
          return;
      }
      if(buttons.length > 0){
          [].forEach.call(buttons, function(element, index){
              element.addEventListener(typeEvent, function(event){
                  if(element.classList.contains("is-active")){
                      element.classList.remove("is-active");
                      mobMenu.classList.remove("is-show");
                      body[0].classList.remove("page_overflow");
                      setTimeout(function(){
                          mobMenu.classList.remove("is-open");
                      },timeTransition);
                  } else {
                      element.classList.add("is-active");
                      mobMenu.classList.add("is-open");
                      body[0].classList.add("page_overflow");
                      setTimeout(function(){
                          mobMenu.classList.add("is-show");
                      },20);
                  }
                  event.preventDefault();
                  return false;
              });
          }, false);
      }
    var $menu =  $("#mobile-menu");
    $menu.mmenu({
          offCanvas: false,
      }, {
          language: "ru"
      });


    var $panels = $menu.find( ".mm-navbar" );  
    $panels.each(function (index) {
        if(index == 0) return;
        var $title =  $(this).find(".mm-navbar__title");
        var titleText = $title.text();
        $title.text('Назад');
        $(this).after("<p class='mm-navbar__header-title'>"+titleText+"</p>")
    }); 
     
};

$(function(){
  initMobMenu();
});